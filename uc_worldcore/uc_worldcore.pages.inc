<?php

/**
 * @file
 * User page callbacks for the WorldCore module.
 */

/**
 * Worldcore payment success page.
 */
function uc_worldcore_success() {
  return theme('worldcore_payment_success');
}

/**
 * Worldcore payment failed page callback.
 */
function uc_worldcore_fail() {
  return theme('worldcore_payment_fail');
}

/**
 * Worldcore status_url processing.
 */
function uc_worldcore_status() {

  drupal_add_http_header('Content-type', 'text/html; charset=UTF-8');

  $headers = apache_request_headers();
  $json_body = file_get_contents('php://input');
  $hash_check = strtoupper(hash('sha256', $json_body . variable_get('uc_worldcore_api_password', '')));
  // Hash checking.
  if ($headers['WSignature'] == $hash_check) {

    // Some code to confirm payment.
    $decoded_response = json_decode($json_body, TRUE);

    $order = uc_order_load($decoded_response['invoiceId']);

    if ($order) {

      if ($decoded_response['status'] == 'Paid') {

        $comment = t('WorldCore track: @track_id', array('@track_id' => $decoded_response['track']));
        uc_payment_enter($decoded_response['invoiceId'], 'uc_worldcore', $decoded_response['amount'], $order->uid, NULL, $comment);
        uc_cart_complete_sale($order);
        uc_order_comment_save($decoded_response['invoiceId'], 0, t('WorldCore payment successful.'));

      }

    }

  }
  else {

    // Some code to log invalid payments.
    watchdog('WorldCore',
    'Hash mismatch! %WSignature vs. %hash_check',
    array(
      '%WSignature' => $headers['WSignature'],
      '%hash_check' => $hash_check,
    ),
    WATCHDOG_ERROR);

    die();

  }

}
